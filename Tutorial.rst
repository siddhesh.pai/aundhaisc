Getting Started on the ISC design
=================================
To redo the A1 ISC design, it wll be necessary to understand:

#. the physcs of laser interferometry
#. moulaton / demodulaton for signal extraction
#. feedback loops (MIMO & SISO)
#. how to use Finesse/pyKat to smulate these thngs




How does Finesse work?
=======================
`Interferometer Sim Tutorial <https://dcc.ligo.org/LIGO-G1901578>`_




How does Laser Interferometry Work?
------------------------------------

* `Intro to GW Detectors <https://dcc.ligo.org/LIGO-G1701153>`_
* `Intro to Controls <https://dcc.ligo.org/LIGO-G1601417>`_







Howto / Syntax on .RST files / misc:
----------------------------------------------------------

* https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html
* `LSC Beginners Guide <https://dcc.ligo.org/LIGO-P1400033>`_