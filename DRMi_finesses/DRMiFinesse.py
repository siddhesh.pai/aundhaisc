# collecting the scripts developed in transmission_optimization (and elsewhere), as used in CavFinesseChanges and then imprroved upon.

from pykat import finesse
from pykat.commands import *
import pykat.external.peakdetect as peak
import pykat.ifo.aligo as aligo
import pykat.ifo.aligo.plot as aligoplt
import gwinc as gwinc
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from pykat.external.progressbar import ProgressBar
from scipy.interpolate import interp1d

# pykat.init_pykat_plotting(dpi=90)

####################### 

####################### Direct Copies from CavFinesseChanges (improved versions above)
def getPF(aligokat,printit=False):
    k=aligokat.deepcopy()
    k.parse("""
    pd PX nETMX1
    pd PS nSRM1
    pd PP nPRM2
    cp cavPRX x finesse
    cp cavSRX x finesse
    cp cavXARM x finesse
    """)
    k.noxaxis=True
    o=k.run()
    if printit:
        print("""
    Cav:      Finesse:     Pcirc:
    Xarm      {:.3g}       {:.3g}
    PRX       {:.4f}       {:.4g}
    SRX       {:.4f}       {:.4g}
        """.format(o["cavXARM_x_finesse"], o["PX"],
                  o["cavPRX_x_finesse"], o["PP"],
                  o["cavSRX_x_finesse"], o["PS"]))
    
    return [o["cavXARM_x_finesse"], o["PX"],
                  o["cavPRX_x_finesse"], o["PP"],
                  o["cavSRX_x_finesse"], o["PS"]]


def printTs(aligokat):
    k = aligokat.deepcopy()
    print("""
    Optic:       Transmission:      Loss
    ITMX         {:.4g}             {:.4g}
    ITMY         {:.4g}             {:.4g}
    PRM          {:.4g}             {:.4g}
    SRM          {:.4g}             {:.4g}
    """.format(float(k.ITMX.T), float(k.ITMX.L), float(k.ITMY.T), float(k.ITMY.L), float(k.PRM.T), float(k.PRM.L), float(k.SRM.T), float(k.SRM.L)))

def tuneT(LIGOkat,optic='PRM',node='nETMX1',val=0.03,precision=0.025,
          target_precision=0.001,target='max',debug=False):
    """
    scans and optimizes the power at a target node 
    by adjusting the transmissivity of an optic.
    Aims to function much like IFO.scan_to_precision, 
    but for mirror tranmissivities rather than tunings.
    assumes optical loss should be unchanged.
    
    Warning: 
    It's slow! The ifo is re-tuned using setup() for 
    each transmissivity to ensure it's in an operational 
    state.
    
    val = initial value at center of search range
    precision = initial range of search
    target_precision = last step range of search. Setting
            target_precision=None turns off the loop.
    target = power that should be measured at that node.
            Should be a float, or 'max' if aim is to
            maximise, whatever the final value might be.       
    
    """
    if target_precision == None:
        target_precision=precision
    while precision>=target_precision:
        #1. scan over the initial range of T-values
        Pvals=[]
        Tvals = np.linspace(val-precision,val+precision,40)
        for tt in Tvals:
            k = LIGOkat.deepcopy()
            k.parse("pd Pow {}".format(node))
            k.noxaxis=True
            k.components[optic].setRTL(1-tt-k.components[optic].L, tt,k.components[optic].L)
            #running setup to ensure the ifo is on resonance
            k = aligo.setup(k) # not lock-dragging: we're comparing 2 diff ifos here not drifting states within 1
            o=k.run()
            Pvals.append(o['Pow'])
        #2. find the T-value for which the power is closest to the target
        if target=='max':
            T_out = Tvals[np.argmax(Pvals)]
            P_out = np.max(Pvals)
        else:
            #compare all vals to the target and find the minima
            diffs = [np.abs(P-target) for P in Pvals]#np.abs(Pvals-target)
            _max,_min = peak.peakdetect(diffs,Tvals,1) #not sure we really need a fancy peak detector here...
            Topts = [p[0] for p in _min] #X-vals for the minima. p[1] contains the values of those minima
            Popts = [Pvals[list(Tvals).index(x)] for x in Topts] #Y-vals corresponding to the minima
            #if there's more than one closest match, select the one thats closest to the originally considered value of X (center)
            diffs2 = [abs(x-val) for x in Topts]
            T_out=Topts[np.argmin(diffs2)]
            P_out=Popts[np.argmin(diffs2)]    
        if debug:
            plt.figure()
            plt.plot(Tvals,Pvals,'-x')
            plt.plot(T_out,P_out,'o')
            plt.xlabel("{} transmission".format(optic))
            plt.ylabel("power at {}".format(node))
            plt.show(block=0)
        val = round(T_out,4)
#         precision = 3*abs(Tvals[1]-Tvals[0]) #new precision looks over 3 steps either side of last run
        precision /= 5
    return T_out, P_out


def quicktuneT2(LIGOkat,optic='PRM',node='nETMX1',val=0.03,precision=0.025,
          target_precision=0.001,target='max', res=40, debug=False):
    #updated func using xaxis on transmission - can't run setup each step this way, and can't lock drag if use single run for it.
    #this version 2 always opts for the second crossing point if matching a target, rather than whichever is least different
    """
    scans and optimizes the power at a target node 
    by adjusting the transmissivity of an optic.
    Aims to function much like IFO.scan_to_precision, 
    but for mirror tranmissivities rather than tunings.
    assumes optical loss should be unchanged.
    
    Warning: 
    It's slow-ish. But importantly, setup() is NOT run
    between T-values, so check it's accuracy vs the above first!
    The first run should always cover as much of the possible 
    T-value range as possible
    
    val = initial value at center of search range
    precision = initial range of search
    target_precision = last step range of search. Setting
            target_precision=None turns off the loop.
    target = power that should be measured at that node.
            Should be a float, or 'max' if aim is to
            maximise, whatever the final value might be. 
    res = number of steps per run (choose higher if not iterating!)
    
    """
    if target_precision == None:
        target_precision=precision
    while precision>=target_precision:
        ##1. scan over the initial range of T-values
        k=LIGOkat.deepcopy()
        k.removeBlock("locks")
        k.parse("""
        xaxis {} T lin {} {} {}
        func refl = 1 - {:.0f} - ($x1)
        put {} R  refl
        pd Pow {}
        """.format(optic,val-precision,val+precision,res,float(k.components[optic].L),optic,node))
        o=k.run()
        Tvals=o.x
        Pvals=o['Pow']
            
        ##2. find the T-value for which the power is closest to the target
        if target=='max':
            T_out = Tvals[np.argmax(Pvals)]
            P_out = np.max(Pvals)
            if debug:
                plt.plot(Tvals,Pvals,'-x')
        else:
            #compare all vals to the target and find the minima
            diffs = [np.abs(P-target) for P in Pvals]#np.abs(Pvals-target)
            _max,_min = peak.peakdetect(diffs,Tvals,1) #not sure we really need a fancy peak detector here...
            Topts = [p[0] for p in _min] #X-vals for the minima. p[1] contains the values of those minima
            Popts = [Pvals[list(Tvals).index(x)] for x in Topts] #Y-vals corresponding to the minima
#             #if there's more than one closest match, select the one thats closest to the originally considered value of X (center)
#             diffs2 = [abs(x-val) for x in Topts]
#             T_out=Topts[np.argmin(diffs2)]
#             P_out=Popts[np.argmin(diffs2)]    
            # if there's multiple minima, pick the one with higher transmission
            if debug:
                print(Topts)
                plt.plot(Tvals,Pvals,'-x')
            T_out = Topts[-1]
            P_out = Popts[-1]
        if debug:
            plt.plot(T_out,P_out,'o')
            plt.xlabel("{} transmission".format(optic))
            plt.ylabel("power at {}".format(node))
            plt.show(block=0)
        val = round(T_out,4)
#         precision = 3*abs(Tvals[1]-Tvals[0]) #new precision looks over 3 steps either side of last run
        precision /= 5
    return val, P_out

def DARM_TF(_kat,optics=["ETMX","ETMY"],label=None,maxtem=None,
            ls='-',lw=1,plotit=False, printit = True,getfp = False):
    #
    """
    from squeezing_MM LLO ifo_design_tools study. 
    Updated Feb 2020 to specify which fpole method you use. 
    updated version of old_DARM_TF. outputs two pole values:
    fp1 = freq where sig amplitude drops to max/√2 (NB sqrt missing from old_DARM_TF, seems to be the main issue there)
    fp2 = freq where sig phase changes by -45deg *relative to starting value*
    
    commands:
    _kat   = supplied finesse model (will be deepcopied for use)
    optics = optics to be differentially shaken
    label  = title if plotted
    maxtem = max HOM order used by simulation (otherwise uses the one set in _kat)
    ls, lw = plotting instructions
    plotit = show the plot resulting from the calculations
    getfp = return the pole frequency as calculated via both methods above. 
            if True, returns out.x, out['TF'], fp, fp2    """
    kat = _kat.deepcopy()
    kat.ETMX.mass = np.inf
    kat.ITMX.mass = np.inf
    kat.ETMY.mass = np.inf
    kat.ITMY.mass = np.inf
    kat.parse("""
    fsig darm  {ax} 1 0
    fsig darm2 {ay} 1 180
    xaxis darm f log 1 10k 1000 #all fsigs get same freq so don't need a put here
    yaxis log abs:deg
    pd1 DARM_TF 1 nAS
    put DARM_TF f $x1
    """.format(ax=optics[0],ay=optics[1]))
    if maxtem != None:
        kat.maxtem = maxtem   
    out = kat.run()
    
    if plotit:
        fig = out.plot(title=label,detectors=["DARM_TF"],show=False,return_fig=True)
        
    if getfp:
    
        def M1(xx,YY):
            #Method 1: from power drop
            Y = abs(YY)
            f = interp1d(Y,xx)
            Ymax = max(Y)
            ymaxidx = list(Y).index(Ymax)
            Xmax = out.x[ymaxidx]
            Yfp = Ymax/np.sqrt(2)
            fp = f(Yfp)
            if printit:
                print("fp  = {:.3f}Hz [amplitude drops to 1/sqrt(2) from max value]".format(fp))
            return fp,Yfp,Xmax,Ymax
        
        def M2(xx,YY):
            #Method 2: from phase change - dependent on starting well below pole freq (uses phi_0 reference)
            Yphi = np.angle(YY,deg=True)
            Ytarget = Yphi[0]-45
            if Ytarget>min(Yphi) and Ytarget<max(Yphi):#NEW: stops the whole thing sometimes-crashing for maxtem 4.
                f2 = interp1d(Yphi,xx)
                fp2 = f2(Ytarget)
            else:
                print("pole finder: target {}deg outside range [{},{}]. attempting to extrapolate instead...".format(
                    Ytarget,min(Yphi),max(Yphi)),end='')
                f2 = interp1d(Yphi,xx,bounds_error=False,fill_value="extrapolate")
                fp2 = f2(Ytarget)
            if printit:
                print("fp2 = {:.3f}Hz [phase shifts by -45deg from initial value]".format(fp2))
            return fp2,Ytarget
        
        if getfp=='fp1':
            fp1,Yfp,Xmax,Ymax = M1(out.x,out["DARM_TF"])
            if plotit:
                ax0 = fig.axes[0]
                ax0.loglog(Xmax,Ymax,'ro',label='peak')
                ax0.loglog(fp1,Yfp,'kx',label="f_p = {:.3f}Hz\n[amp=max/√2]".format(float(fp1)))
                ax0.legend(loc=2,bbox_to_anchor=(1,1))
                plt.show()
            return fp1
            
        if getfp=='fp2':
            fp2,Yfp2 = M2(out.x,out["DARM_TF"])
            if plotit:
                ax1 = fig.axes[1]
                ax1.semilogx(fp2,Yfp2,'kx',label="f_p2 = {:.3f}Hz\n[phase=$\phi_0$-45deg]".format(float(fp2)))
                ax1.legend(loc=2,bbox_to_anchor=(1,1))
                plt.show()
            return fp2
        
        if getfp=='both':
            fp1,Yfp,Xmax,Ymax = M1(out.x,out["DARM_TF"])
            fp2,Yfp2 = M2(out.x,out["DARM_TF"])
            if plotit:
                ax0 = fig.axes[0]
                ax0.loglog(Xmax,Ymax,'ro',label='peak')
                ax0.loglog(fp1,Yfp,'kx',label="f_p = {:.3f}Hz\n[amp=max/√2]".format(float(fp1)))
                ax0.legend(loc=2,bbox_to_anchor=(1,1))
                ax1 = fig.axes[1]
                ax1.semilogx(fp2,Yfp2,'kx',label="f_p2 = {:.3f}Hz\n[phase=$\phi_0$-45deg]".format(float(fp2)))
                ax1.legend(loc=2,bbox_to_anchor=(1,1))
                plt.show()
            return [fp1,fp2]
            
        
    else: return out.x,out["DARM_TF"]
                
def tuneTfp_I2(_kat,target=450,optic='SRM',loss = 0, Tsteps=8, fprecision=1, Tthresh=1e-4,
            debug=False,deepdebug=False,fpmethod='phase',verbose=False):
    """
    scans the transmission of the specified optic 
    and measures the DARM pole, then interpolates to 
    find the exact transmission that gives the target pole freq.
    
    target = DARM pole that should be measured.
    optic = optic whose transmission we vary
    loss = static loss on that optic
    Tsteps = number of points in initial scan
    debug = show plot
    deepdebug = show plots of each pole measurement
    fpmethod = method used to identify pole frequency (phase or amplitude)
    verify = re-run Finesse using the value from interpolation to check the result
    fprecision = max difference between fp_out and target value allowed
    Tthresh = threshold of narrowest range of transmission values to consider
    verbose=text output
    
    """
#     print("\n>>version 4")
    if verbose:
        print("""
        reminder: 
        running with maxtem {} and setup() after each step. this will be slow!
        """.format(_kat.maxtem))
    
    if fpmethod=='amplitude':
        fpm = 'fp1'
    else:
        fpm = 'fp2' #defaults to using phase method
        
    if debug:
        fig,ax=plt.subplots()
    
    newpole = 0
    rng=1
    jj=0
    Topt=None
    while abs(newpole-target)>fprecision:
        if verbose:
            print("stage {}".format(jj))
    
        if Topt:
            Tvals = np.linspace(Topt-rng, Topt+rng,Tsteps)
        else: #first run: small offsets from top and bottom so we always have pretuning signals for setup()
            Tvals = np.linspace(1e-6,1-1e-6-loss,Tsteps)
            
        if max(Tvals)-min(Tvals) < Tthresh:
            if verbose:
                print('Tval range narrower than threshold ({}), stopping.'.format(Tthresh))
            return Topt
        
        else:
            poles = []
            
            if verbose:
                pb = ProgressBar()
                pb.maxval = len(Tvals)

            i=0
            for T in Tvals:
                k=_kat.deepcopy()
                k.components[optic].setRTL(1-T-loss,T,loss)
                k = aligo.setup(k)
                poles.append(DARM_TF(k,optics=["LX","LY"],plotit=deepdebug,printit=False,getfp ='fp2'))
                i+=1
                if verbose:
                    pb.update(i)
            if debug:
#                 #for now to check the initial run
#                 f,a=plt.subplots()
#                 a.plot(Tvals,poles,label='stage {} full run'.format(jj))
#                 f.show()
                #for the full stepping-in plot
                ax.plot(Tvals,poles,label='stage {} full run'.format(jj))
                
            if target>min(poles) and target<max(poles): ###NEW. attempt to resolve m4 crashes, but tell me when its doing it.
                f = interp1d(poles,Tvals)
                Topt = f(target)
            else:
                print("target of {}Hz outside interpolation bounds [{},{}]. attempting to *extrapolate* T_opt.".format(
                target, min(poles),max(poles)))
                f = interp1d(poles,Tvals,bounds_error=False,fill_value="extrapolate")
                Topt = f(target)


            if debug:
                testffs = np.linspace(min(poles),max(poles),Tsteps*10)
                ax.plot(f(testffs),testffs,'--',lw=2,alpha=0.5,label='stage {} interpolated line'.format(jj))
                ax.plot(Topt,target,'x',label='stage {}: T={:.3f}'.format(jj,Topt))
            
            if verbose:
                print('verifying...')
            k=_kat.deepcopy()
            k.components[optic].setRTL(1-Topt-loss,Topt,loss)
            k = aligo.setup(k)
            newpole = DARM_TF(k,optics=["LX","LY"],plotit=deepdebug,printit=False,getfp ='fp2')
            if verbose:
                print('with T={:.4f}, found pole: {:1f}Hz; target was {:.1f}Hz'.format(Topt,newpole,target))
            if abs(newpole-target)>fprecision:
                if verbose:
                    print('new value is more than {:g}Hz away from target. Running again over narrower range.'.format(fprecision))
                rng/=10
                jj+=1

        if debug:
            ax.set_xlabel('{}_T'.format(optic))
            ax.set_ylabel('DARM pole [Hz]')
            ax.legend()
            fig.show()
            

    return Topt, newpole
       